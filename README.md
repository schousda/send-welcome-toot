# Send-Welcome-Toot 

This Python script sends a toot to welcome new users.

# Installation
Clone this repository or download and unzip the source code. 

# Configuration
Rename the config_examples.json to config.json
After this you need to add your information to the file.

## Mastodon
Please make sure you have created a Mastodon App.
In your account go to *Preferences - Development* and create a new application. 
Set this scope:

 - write:statuses
 - admin:read:accounts

 **Please handle the credentials with care, since this app could access personal user data.**

After you created the app open the application and get the client key, client secret and your access token. Add this information the the configuration file.

- `client_id`: Add the Mastodon client ID here 
- `client_secret`: Add the Mastodon client secret here 
- `access_token`: Add the Mastodon access token here 
- `instance_url`: Add your instance URL
- `template`: Here you can specify the text to be tooted. The template has to include `"new_accounts"`.
- `visibility`: `"direct"` - post will be visible only to mentioned users `"private"` - post will be visible only to followers `"unlisted"` - post will be public but not appear on the public timeline `"public"` - post will be public


## Usage
Run the script with `python3 ./send_welcome_toot.py` or use the bash file to run it as a cronjob. Please don't forget to edit the path to your script there.
Already welcomed users are added to welcome.json by the script.