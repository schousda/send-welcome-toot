from mastodon import Mastodon
import json
import re


# Attempt to open file before accessing to ensure we have permission
def open_file(fname, perms):
    try:
        file = open(fname, perms)
        return file
    except OSError:
        return None


# read configuration file
def get_parameters(config_file):
        config_file = open_file(config_file, "rb",  )
        if config_file is not None:
                configuration = json.load(config_file)
                config_file.close()
        if config_file is None:
                print("No configuration file found!\n")
        return configuration

# authenticate to mastodon
def connect_mastodon(config):
    if not config['instance_url'].startswith(('http://', 'https://')):
        config['instance_url'] = 'https://' + config['instance_url']
    mastodon_api = Mastodon(
        api_base_url=config['instance_url'],
        client_id=config['client_id'],
        client_secret=config['client_secret'],
        access_token=config['access_token']
        )
    return mastodon_api

# get mastodon accounts from API
def get_mastodon_accounts(mastodon):
        accounts = mastodon.admin_accounts(status='active')
        return accounts

# only keep id and username
def clean_list(mastodon_accounts):
        m_account = []
        m_accounts = []
        for m_account in mastodon_accounts:
                if m_account['approved'] == False:
                        continue
                m_account = {key: m_account[key] for key in m_account.keys() & {'id','username'}}
                m_accounts.append(m_account)
        return m_accounts

# compare to find new accounts
def new_accounts(m_accounts, w_accounts):
        new_accounts = []
        for account in m_accounts:
                if account not in w_accounts:
                        new_accounts.append(account)
        return new_accounts

# send welcome toot
def build_toot_text(new_accounts, config, mastodon):
        pattern_colon = r':[^:\s]*(?:::[^:\s]*)*:'
        pattern_brace = r'{[^:\s]*(?:::[^:\s]*)*}'
        text_length = len(re.sub(pattern_brace,'',re.sub(pattern_colon, '##', config['template'])))
        accounts = ''
        for new_account in new_accounts:
                accounts_length = len(accounts + '@' + new_account['username'] + ' ')
                print('accounts length: ' + str(accounts_length))
                toot_length = text_length + accounts_length
                print('toot length: ' + str(toot_length))
                if toot_length > 500:
                        toot_welcome(accounts, config, mastodon)
                        accounts = '@' + new_account['username'] + ' '
                else:
                        accounts = accounts + '@' + new_account['username'] + ' '
        if accounts:
                toot_welcome(accounts, config, mastodon)
        return
        
# send toot        
def toot_welcome(accounts, config, mastodon):
        new_accounts = {}
        new_accounts['new_accounts'] = accounts
        mastodon.status_post(config['template'].format(**new_accounts)[:499], visibility=config['visibility'])
        return

# update welcomed users file
def update_welcomed_accounts(welcomed_accounts, new_users, welcomed_file):
        welcomed_file = open_file(welcomed_file, "w")
        if welcomed_file is not None:
                for user in new_users:
                        welcomed_accounts.append(user)
                welcomed_file.write(json.dumps(welcomed_accounts, indent=4))
                welcomed_file.close()
        return

if __name__ == "__main__":
        config_file = 'config.json'
        welcomed_file = 'welcomed.json'
        config = get_parameters(config_file)
        welcomed_accounts = get_parameters(welcomed_file)
        mastodon = connect_mastodon(config)
        mastodon_accounts = get_mastodon_accounts(mastodon)
        users = clean_list(mastodon_accounts)
        newaccounts = new_accounts(users, welcomed_accounts)
        if newaccounts:
                print('neue user gefunden')
                build_toot_text(newaccounts, config, mastodon)
                update_welcomed_accounts(welcomed_accounts, newaccounts, welcomed_file)
        else:
                print('No new accounts found')